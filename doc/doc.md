---
title: Comparison of parallel programming in Go and CUDA
author: Andreas Reindl
header-includes: |
  \usepackage[utf8]{inputenc}
  \usepackage{setspace}
  \setstretch{1.2}
...

# Introduction

Up to the early days of integrated circuits, Central Processing Units (CPUs) had only one core and Graphics Processing
Units (GPUs) didn't even exist.
In order to get more and more processing power into CPUs (and keep Moores law alive), CPU manufacturers started adding
more cores into one CPU.
But because of the single core history, many programming languages were historically developed with only one core in
mind and had to find a way to add multi-threading after the fact.
Some languages have been more successful than others in terms of their simplicity and built-in features.

# Architectures

CPUs and GPUs are built for very different purposes.
Where CPUs are used to do general and many different computations at an high speed, GPUs can do the same operation
on many different data points at the same time.
This is reflected in the hardware layout and architecture of these two systems.

## CPU

In order to execute many different operations a CPU has many different instructions it can use in the hardware.
But this requires a huge amount of physical space on the silicon-die and so it results in a small amount of cores
because of space restrictions and commercial manufacturing reasons.

The upside of these huge cores is that the CPU can compute even complicated operations in just a few CPU cycles.
Combined with fast clock speeds, this leads to the relatively efficient execution of general purpose computations.

In comparison to GPUs, the layout of the hardware in CPUs is much more disconnected.
A modern CPU has 4 levels of memory that differ in terms of operation speeds and how many cores share the same memory.
While the main memory (RAM) is shared amongst all cores, mostly half of the cores have access to the same L3 cache and
fewer cores share the same L2 cache (modern architectures like ZEN4 from AMD have a unique L2 cache for each core).
The fastest cache, the L1 cache, is unique for every core.
This hardware limitation makes migrating a process from one core to another likely to be very costly in terms of
execution time.
Because the new core most likely has to access the L3 cache or in the worst case even the main memory for new
instructions and data.

Apart from the complicated memory layout, the RAM is physically very far away from the CPU in comparison to the GPU and
much slower than the GPU main memory.

## GPU

In contrast to the few but powerful cores of CPUs, GPUs have many small cores, built for one specific purpose: compute
mathematical operations.
Depending on the definition of core, GPUs have much less cores than the manufacturers say.
If a core has to have a unit that can fetch, decode and schedule instructions and fetch the data these instructions
need, then a GPU has much less cores.
What differentiates them from CPU cores most is that GPU cores can run many operations at the same time, even on the
same core.

The GPU core hierarchy is a bit more complicated.
NVIDIA GPUs for example are composed of Graphics Processing Clusters (GPCs) that contain a few Streaming
Multiprocessors (SMs).
These SMs have the instruction- and data-cache that can be used by clusters of 32 CUDA cores, that itself are
called Warps.
A Warp contains, similar to a CPU core, cache and the scheduling capabilities that are used to control the CUDA cores.
The upside of this approach is that a Warp scheduler only has to fetch a instruction once and can apply the same
instruction to multiple data points at the same time.
But the downside is that all threads that are scheduled in this Warp have to agree on the instruction they want to
execute.
If some threads branch and want to execute different instructions, these threads have to wait to be scheduled instead
of the other threads.

![GP100 Block Diagram](assets/gp100_block_diagram-1.png)
Figure 1: NVIDIA GP100 Block diagram [\[5\]][5]

![GP100 Streaming Multiprocessor](assets/gp100_SM_diagram.png)
Figure 2: NVIDIA GP100 Streaming Multiprocessor [\[5\]][5]

Even if the scheduling and control requirement is not needed, CUDA cores are still much less powerful than CPU cores.
Because CPUs have to do a wide range of different workloads and GPUs are specialized for mathematical operations, CUDA
cores contain less instructions in their hardware.
This makes them simpler and each core therefore needs less space on the die.
So more cores can be fitted onto one GPU.

GPUs are specialized to crunch numbers very quickly, which is why they need very fast memory.
The GTX 1080ti for example has its memory chips directly next to the GPU die on the PCB, connected with a 352 bit bus
that results in a transfer rate of 484 GB per second.
In contrast to that, a modern CPU has a bus of 64 bit and a transfer rate of around 20 GB per second.
This property of GPUs also reflect in the cache sizes.
They are lower than on CPUs because the main memory is really fast and a GPU usually works with a lot of different data
that it isn't efficient to store a large amount near the cores.
To come back to the 1080ti example, it only has 48 KB of L1 cache for each SM abd 2.75 MB of L2 cache on the whole card.
To put this into contrast, a Ryzen 9 5950X has 64 MB of L3 cache, which eight cores share, and 512 KB of L2 or
respectively 64 KB of L1 in each core.

# Programming Models

As written in the [architectures chapter](#architectures), the purpose of CPUs differ from GPUs.
On CPUs you want to be able execute many different operations and mostly have only a single or a few data points that
needs the same instruction.
The first programming model is called SISD (single instruction, single data) and the other one SIMD
(single instruction, multiple data).
CPUs have instructions for both of these models, but SIMD is only supported for a few specific types of work.

GPUs use a slightly different model than SIMD called SIMT (single instruction, multiple threads).
The main difference is that the SIMD instructions in CPUs use one core to apply the same operation on many data points
and GPUs use many cores, but they also have some SIMD instructions that can be used for certain operations.
Where CPUs use a few but powerful cores, GPUs can throw a huge amount of cores onto an large amount of data.

Another difference how CPUs and GPUs are programmed is the amount of and the level at which schedulers decide what data
and operation which core has to process.
While both have schedulers in the hardware (CPUs: per core, GPUs: one for many cores), a programmer may encounter a few
more.
Firstly, there is the scheduler of the operating system (OS) which is managed by the OS and, for example in the case of
Go, there is also the internal scheduler of Go.
These levels also represent different levels of control by the programmer.
Hardware schedulers can't be influenced by the programmer, but the OS scheduler may have some parameters the user can
tweak and it listens for signals that another thread can be scheduled because the current thread has no work to do or
has to wait for something.

The following subchapters explain the different mindset a programmer must have when working on an GPU in contrast to
programming for a CPU and compares system thread/processes with goroutines.

## Goroutines

In Go there is a M to N correlation of system threads to goroutines.
When the Go runtime bootstraps itself, it starts a predefined amount of system threads which defaults to the number of
processors on the system.
The programmer then can start as many goroutines as they want during program execution.

A goroutine can be seen as lightweight thread that is managed by its own scheduler, the Go runtime.
This scheduler differs from the system scheduler in two aspects.
It is a cooperative work-stealing scheduler in contrast to the mostly preemptive schedulers in operating system kernels.
In non-kernel-developer terms this means, that a running goroutine can't be interrupted by the scheduler, or other
goroutines, and if one of the system threads runs out of goroutines it "steals" goroutines from other threads.

It is really simple for the programmer to start a goroutine.
They just have to define a function and prefix the call of it with `go`:

```go
func main() {
  go myGoroutine()
}

func myGoroutine() {
  fmt.Println("Hello World")
}
```
Code 1: Simple example of a goroutine

This can be that simple because goroutines are smaller than system threads.
They need much less memory and don't have as much metadata associated with them which makes it easier to migrate them to
a new executor or thread.
Migrating tasks from one CPU core to another is not always a good idea, for example when it gets shared across NUMA
boundaries (e.g. 2 CPUs with distinct memory pools), but explaining this to the needed detail is out of scope of this
work.

### Communication between Goroutines

In order to establish some kind of communication and synchronization the only method available to Go programmers are
channels.
A channel is a communication structure where you can put data into one side that comes out at the other end which is
mostly another goroutine.
If the channel is unbuffered, what they are by default, it can also be used to let another goroutine wait until one has
finished its calculations.

```go
func main() {
  // create a channel that can transport a string
  c := make(chan string)

  // start a goroutine that generates a string
  // and give the sending end of the channel to it
  go generator(c)

  // wait for the goroutine to finish generating and print the resulting string
  fmt.Println(<-c)
}

func generator(c chan string) {
  time.Sleep(1 * time.Second)

  // send the generated string through the channel
  c <- "Hello World"
}
```
Code 2: Example of a communication between two goroutines

## GPU Programming

When programming a GPU a developer has to change their mindset in a drastic way.
While a CPU mostly runs sequential code with some branching, a GPU is, as explained above, optimized for highly parallel
processing.
Code that runs on CUDA GPUs is called a kernel because it is the inner most part of a parallel program.
It can be thought of as the inner part of a for-loop that can be parallelized:

```go
func foo(a, b, c [32][32]int) {
  // for every row
  for x := 0; x < 32; x++ {
    // for every column
    for y := 0; y < 32; y++ {
      // this can be parallelized
      tmp := 0
      for k := 0; k < 32; k++ {
        tmp += a[x][k] + b[k][y]
      }

      c[x][y] = tmp
    }
  }
}
```
Code 3: Example of a parallelizable Go code with for loops

### Threads, Blocks and Grids

Instead of iterating with for-loops the kernel first has to compute where its workload is.
This is done via the block and thread values.
These values are set by the programmer when the CUDA kernel gets called in the code and basically tells the GPU how many
times the kernel should be executed.

```cu
__global__ void matrix_mul(const int *a, const int *b, int *c, int N) {
  // get global indices
  int row = blockIdx.y * blockDim.y + threadIdx.y;
  int col = blockIdx.x * blockDim.x + threadIdx.x;

  // compute a value in output array
  int result = 0;
  for (int k = 0; k < N; k++) {
      result += a[row * N + k] * b[k * N + col];
  }

  // write to output array
  c[row * N + col] = result;
}
```

```cu
matrix_mul<<<grid, threads>>>(a, b, c, 1024);
```
Code 4: Example of a CUDA kernel and the call of it

The value inside the three angle brackets of the CUDA kernel call represents combined the number of threads that should
be started.
A grid is the CUDA terminology for a group of blocks.
It can be one, two or three dimensional and just helps the programmer to make the calculation of the array or matrix
position easier.
A block is a group of threads that are executed on the same SM.
This is the second part of the angle bracket syntax.
So the call of `matrix_mul` is run by a three dimensional grid of blocks (Dx, Dy, Dz) and (Nx, Ny, Nz) threads for each
dimension of these blocks of the grid.

### Memory management

First of all the memory on the host (the computer the code runs on) and the device (the GPU the CUDA kernels are run)
should be allocated.
It is best practice to prefix memory with `host` or `h` and `device` or `d` respectively to later know these two apart.

```cu
  // matrix dimension
  int N = 1024;

  // size of matrix
  size_t MATRIX_SIZE = N * N * sizeof(int);

  // allocate host arrays
  std::vector<int> host_a(N * N);
  std::vector<int> host_b(N * N);
  std::vector<int> host_c(N * N);

  // allocate device memory
  int *device_a, *device_b, *device_c;
  cudaMalloc(&device_a, MATRIX_SIZE);
  cudaMalloc(&device_b, MATRIX_SIZE);
  cudaMalloc(&device_c, MATRIX_SIZE);
```
Code 5: Memory allocation on host and device

There are two ways of allocating memory for CUDA code.
You can use unified memory with `cudaMallocManaged(&pointer, size)` which gives the benefit that you can access the same
pointer from both the host and the device.
The memory is transparently managed by the host and the device in the form of copying data to the side that requests it
when it was modified on the other side.
But this can make the runtime unpredictable because the GPU may has to wait a long time until the data is copied from
the host memory to its own.

A faster way is to manage the memory on each side manually with `cudaMalloc` (see code example 5).
The downside of this approach is that you also have to manually copy the memory and free it afterwards:

```cu
  // copy arrays to the device
  cudaMemcpy(device_a, host_a.data(), MATRIX_SIZE, cudaMemcpyHostToDevice);
  cudaMemcpy(device_b, host_b.data(), MATRIX_SIZE, cudaMemcpyHostToDevice);

  // do computations

  // wait for device to finish and copy result array back to the host
  cudaMemcpy(host_c.data(), device_c, MATRIX_SIZE, cudaMemcpyDeviceToHost);

  // (do more computations)

  // free memory on device
  cudaFree(device_a);
  cudaFree(device_b);
  cudaFree(device_c);
```
Code 6: Example of manual memory management

Because the GPU runs asynchronously the code on the host has be synchronized with the threads running on the device.
This can be either done with implicitly with calls to `cudaMemcpy` or explicitly with `cudaDeviceSynchronize()`.

To call a CUDA kernel you have to compute the before mentioned grid of blocks and corresponding threads.
This has to be done exact or the kernel needs a bounds check.
The following example uses the hardcoded value of 32 for each block dimension, because of the warp size of 32 of the
hardware.
Then the number of blocks is calculated by dividing the size of the matrices by the number of threads.
Because the matrices are symmetric the number of blocks (grid) and the threads can be used for both dimensions, this
may require some padding in which case the before mentioned bounds check in the kernel is needed.

```cu
  // 32 is the warp size so all 32 threads should be scheduled at the same time
  int THREADS = 32;

  // blocks per grid dimension (assumes THREADS divides N evenly)
  int BLOCKS = N / THREADS;

  // two dimensional block and grid dimensions
  dim3 threads(THREADS, THREADS);
  dim3 grid(BLOCKS, BLOCKS);

  matrix_mul<<<grid, threads>>>(device_a, device_b, device_c, N);
```
Code 7: Example of thread calculation and CUDA kernel call

# Conclusion

While Go is pretty easy to learn and understand its concurrency principles it is limited to the CPU and its parallelism
capabilities.
Go should be used in the day to day work if your code could profit of a few workers more that tackle the same problem.
In Go the Programmer gets no choice on how to work with concurrency, because there always is just one way to do it.
On the one hand this greatly simplifies concurrent code, but on the other hand, it leaves no room for the programmer to
optimize the code.

If you have a problem that could be solved in a highly parallel way and you want to use the raw computing performance of
graphics cards you should use languages like CUDA or Open-CL.
GPUs provide much higher computational power than CPUs, but the problem must be applicable to the specific programming
model due to the hardware design of these devices.
CUDA is a good example for languages in this area.
It is well developed and mature and gives the programmer much control over the code and the optimizations, that would
be too much to explain for this work.
These optimizations manly focus around data locality (getting the data required for computations near to the executing
cores) and helping the compiler in using the SIMD instructions of the GPU.

The biggest downside of CUDA is the dependence on NVIDIA.
It is really well integrated and the performance doesn't disappoint, but you are forced to use proprietary tools whose
quality is at NVIDIAs mercy.

#### Sources

1. <https://www.techspot.com/article/2363-multi-core-cpu/>
2. <https://www.hardwaretimes.com/amd-ryzen-5000-zen-3-architectural-deep-dive/>
3. <https://youtu.be/IzU4AVcMFys>
4. <https://youtu.be/x-N6pjBbyY0>
5. <https://developer.nvidia.com/blog/inside-pascal/>
6. <https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html>

[5]: <https://developer.nvidia.com/blog/inside-pascal/>
