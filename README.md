# Seminar work for KP

This is a seminar work for KP (Konzepte der Programmiersprachen) at the University
of Applied Sciences Rosenheim.
In this work I compare parallel programming in Go with CUDA.

This repository consists of three folders:

- cuda: the CUDA [source code](cuda/mmul.cu)
- go: the go [source code](go/mmul.go)
- doc: the term paper in form of a [Markdown file](doc/doc.md) and a converted [PDF](doc/doc.pdf)

Both languages compute a matrix multiplication with the standard parallelism
paradigms of the respective language.
You can run them yourself by following the steps below.

## CUDA

CUDA runs only on Nvidia graphics cards so you need one of those and additionally to the
driver for it also the CUDA libraries and tools.
For instructions on how to install them see [the official CUDA docs](https://docs.nvidia.com/cuda/index.html)
(or just install them via your package manager).

After you installed all required tools start by compiling the program:

```bash
cd cuda
nvcc mmul.cu kernels.cu --std=c++11 -o mmul # I used some stuff from the C++ 11 standard library
```

When the program is compiled you can start it:

> start with a lower value than you see here, I used a 1080Ti for my test

```bash
./mmul -m 16384 -c -p
```

Following command line switches are available:

switch | meaning
--- | ---
`-c` | selects the kernel with shared cache (can be combined with `-p`)
`-h` | displays usage information and exits
`-m <matrix_size>` | takes an integer for the matrix size
`-p` | selects the kernel with prefetching (can be combined with `-c`)
`-q` | prints only the time the execution of the kernel on the GPU took in milliseconds

The program then performs the computations and outputs the time the internal kernel
needed for the computation:

```none
size of one matrix: 268435456 elements, 1048576 KiB
compute kernel execution time: 6369.57ms
```

## Go

If you haven't already Go installed start by installing it via your package
manager or if you have Windows do it manually:
[official docs](https://go.dev/doc/install).

After that you can just execute the program with `go run`:

```bash
cd go
go run mmul.go
```

Because of restrictions of Go you have to set the matrix side length in the code.
But Go compiles pretty fast so this shouldn't be a problem.

In the code there is also a naive linear implementation that is only there for
completeness and gets never called. If you want to have a look at the difference
between parallel execution time and this naive implementation just comment the
`goroutines` function out and remove the slashes from `naive`.
