#include "kernels.h"

__global__ void matrix_mul(const int *a, const int *b, int *c, int N) {
  // get global indices
  int row = blockIdx.y * blockDim.y + threadIdx.y;
  int col = blockIdx.x * blockDim.x + threadIdx.x;

  // compute a value in output array
  int result = 0;
  for (int k = 0; k < N; k++) {
      result += a[row * N + k] * b[k * N + col];
  }

  // write to output array
  c[row * N + col] = result;
}

__global__ void matrix_mul_prefetch(const int *a, const int *b, int *c, int N) {
  // get global indices
  int row = blockIdx.y * blockDim.y + threadIdx.y;
  int col = blockIdx.x * blockDim.x + threadIdx.x;

  // compute a value in output array
  int result = 0;
  for (int k = 0; k < N; k += 4) {
    // load four ints at a time of the first vector we traverse linearly
    int4 a_prefetch = reinterpret_cast<const int4*>(&a[row * N + k])[0];

    result += a_prefetch.x * b[k * N + col];
    result += a_prefetch.y * b[(k + 1) * N + col];
    result += a_prefetch.z * b[(k + 2) * N + col];
    result += a_prefetch.w * b[(k + 3) * N + col];
  }

  // write to output array
  c[row * N + col] = result;
}

__global__ void matrix_mul_cached(const int *a, const int *b, int *c, int N) {
  // initialize shared memory
  __shared__ int a_cache[SHARED_MEM_SIZE];
  __shared__ int b_cache[SHARED_MEM_SIZE];

  // get global indices
  int row = blockIdx.y * blockDim.y + threadIdx.y;
  int col = blockIdx.x * blockDim.x + threadIdx.x;

  // simplify code
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int dim = blockDim.x; // each dimension is the same size

  int result = 0;
  // move cache tiles along each axis
  for (int c_idx = 0; c_idx < (N / dim); c_idx++) {
    a_cache[ty * dim + tx] = a[(row * N) + (c_idx * dim) + tx];
    b_cache[ty * dim + tx] = b[(c_idx * dim * N) + (ty * dim) + col];

    // wait for all threads to finish loading data
    __syncthreads();

    // compute a value in output array with current cache
    for (int k = 0; k < dim; k++) {
      result += a_cache[ty * dim + k] * b_cache[k * dim + tx];
    }

    // wait for all threads to finish before loading new data into cache
    __syncthreads();
  }

  // write to output array
  c[row * N + col] = result;
}

__global__ void matrix_mul_prefetch_cached(const int *a, const int *b, int *c, int N) {
  // initialize shared memory
  __shared__ int a_cache[SHARED_MEM_SIZE];
  __shared__ int b_cache[SHARED_MEM_SIZE];

  // get global indices
  int row = blockIdx.y * blockDim.y + threadIdx.y;
  int col = blockIdx.x * blockDim.x + threadIdx.x;

  // simplify code
  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int dim = blockDim.x; // each dimension is the same size

  int result = 0;
  // move cache tiles along each axis
  for (int c_idx = 0; c_idx < (N / dim); c_idx++) {
    a_cache[ty * dim + tx] = a[(row * N) + (c_idx * dim) + tx];
    b_cache[ty * dim + tx] = b[(c_idx * dim * N) + (ty * dim) + col];

    // wait for all threads to finish loading data
    __syncthreads();

    // compute a value in output array with current cache
    for (int k = 0; k < dim; k += 4) {
      // load four ints at a time of the first vector we traverse linearly
      int4 a_prefetch = reinterpret_cast<const int4*>(&a_cache[ty * dim + k])[0];

      result += a_prefetch.x * b_cache[k * dim + tx];
      result += a_prefetch.y * b_cache[(k + 1) * dim + tx];
      result += a_prefetch.z * b_cache[(k + 2) * dim + tx];
      result += a_prefetch.w * b_cache[(k + 3) * dim + tx];
    }

    // wait for all threads to finish before loading new data into cache
    __syncthreads();
  }

  // write to output array
  c[row * N + col] = result;
}
