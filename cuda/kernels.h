const int THREADS = 32; // hardware warp size is 32
const int SHARED_MEM_SIZE = THREADS * THREADS;

__global__ void matrix_mul(const int *a, const int *b, int *c, int N);

__global__ void matrix_mul_prefetch(const int *a, const int *b, int *c, int N);

__global__ void matrix_mul_cached(const int *a, const int *b, int *c, int N);

__global__ void matrix_mul_prefetch_cached(const int *a, const int *b, int *c, int N);
