#include <cstdint>
#include <iostream>
#include <ostream>
#include <string>
#include <vector>

enum class Method : uint8_t {
  NAIVE = 0,
  PREFETCH,
  CACHE,
  PREFETCH_CACHE,
};

struct Args {
  int matrix_size;
  bool quiet;
  bool test_result;
  Method method;
};

void print_help(const std::string name) {
  std::cout << "usage: " << name << " [-m <matrix_size>] [-chpqt]" << std::endl;
  std::exit(0);
}

void exit_error(const char *msg) {
  std::cerr << msg << std::endl;
  std::exit(1);
}

Args parse_args(int argc, char *argv[]) {
  Args args{1 << 10, false, false, Method::NAIVE};

  if (argc > 1) {
    for (int i = 1; i < argc; i++) {
      std::string arg = argv[i];

      if (arg == "-h") {
        print_help(argv[0]);
      } else if (arg == "-q") {
        args.quiet = true;
      } else if (arg == "-t") {
        args.test_result = true;
      } else if (arg == "-p") {
        switch (args.method) {
        case Method::CACHE:
          args.method = Method::PREFETCH_CACHE;
          break;
        default:
          args.method = Method::PREFETCH;
          break;
        }
      } else if (arg == "-c") {
        switch (args.method) {
        case Method::PREFETCH:
          args.method = Method::PREFETCH_CACHE;
          break;
        default:
          args.method = Method::CACHE;
          break;
        }
      } else if (arg == "-m") {
        i++;
        if (i >= argc) {
          exit_error("please input a number after '-m'");
        } else {
          args.matrix_size = std::stoi(argv[i]);
          if (args.matrix_size < 0 && args.matrix_size % 32 != 0) {
            exit_error("matrix size must be positive and divisable by 32");
          }
        }
      }
    }
  } else {
    print_help(argv[0]);
  }

  return args;
}
