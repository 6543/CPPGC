package main

import (
	"fmt"
	"time"
)

// matrix size
const N = 1 << 12

// own types to make code more readable
type ray [N]int
type matrix [N]ray

func main() {
	// allocate memory for all matrices
	matrix_a := new(matrix)
	matrix_b := new(matrix)
	matrix_c := new(matrix)

	// initialize matrix a and b
	for x := 0; x < N; x++ {
		for y := 0; y < N; y++ {
			val := x * y
			matrix_a[x][y] = val
			matrix_b[x][y] = val
		}
	}

	start := time.Now()
	// compute matrix c
	// naive(matrix_a, matrix_b, matrix_c)
	goroutines(matrix_a, matrix_b, matrix_c)

	elapsed := time.Since(start)

	fmt.Print("compute time: ")
	fmt.Println(elapsed)
}

// naive, single threaded implementation
func naive(a, b, out *matrix) {
	// for every row
	for x := 0; x < N; x++ {
		// for every column
		for y := 0; y < N; y++ {
			// for every element compute value of output array
			tmp := 0
			for k := 0; k < N; k++ {
				tmp += a[x][k] + b[k][y]
			}

			// write value to output array
			out[x][y] = tmp
		}
	}
}

// concurrent implementation with channels
func goroutines(a, b, out *matrix) {
	// buffer for channels to later wait on
	channels := new([N]chan ray)

	// for every row start a new goroutine that computes it
	for x := 0; x < N; x++ {
		c := make(chan ray)
		go kernel(a, b, x, c)
		channels[x] = c
	}

	// wait on all computations and write result row to output array
	for x, c := range channels {
		out[x] = <-c
	}
}

func kernel(a, b *matrix, x int, c chan ray) {
	results := new(ray)

	// for every column
	for y := 0; y < N; y++ {
		// for every element compute value of output array
		tmp := 0
		for k := 0; k < N; k++ {
			tmp += a[x][k] + b[k][y]
		}

		// write value to output array
		results[y] = tmp
	}

	// when finished send resulting row back to main thread
	c <- *results
}
